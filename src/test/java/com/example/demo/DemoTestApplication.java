package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoTestApplication {

    @LocalServerPort
    private int port;

    private final TestRestTemplate restTemplate = new TestRestTemplate();

    @Test
    public void testHomeEndpoint() {
        String response = restTemplate.getForObject("http://localhost:" + port + "/", String.class);
        assertEquals("Spring is here!", response);
    }

    @Test
    public void testNonFunctionalEndpoint() {
        String response = restTemplate.getForObject("http://localhost:" + port + "/coucou", String.class);
        assertEquals("This endpoint doesn't work properly", response);
    }

    @Test
    public void testEmpty() {
        // Test qui ne contient rien
    }
}
